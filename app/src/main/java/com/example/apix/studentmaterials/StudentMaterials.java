package com.example.apix.studentmaterials;

import android.app.Application;
import android.util.Log;

/**
 * Created by Apix on 19/03/2017.
 */

public class StudentMaterials extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        TimerChecker.contextIntializer(this);

        TimerChecker.getInstance().startTimer(new TimerChecker.TickListener() {
            @Override
            public void onTick(long baseTime) {
                Log.d("TimeElapsed", timerfunction(baseTime));
            }
        });

    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public String timerfunction(long secondsleft){
        int minutes = (int) secondsleft/60;
        long seconds = secondsleft - minutes*60;
        String secondsString = Long.toString(seconds);
        if (seconds<=9){
            secondsString ="0"+secondsString;
        }
        return (Integer.toString(minutes)+":"+secondsString);
    }
}
