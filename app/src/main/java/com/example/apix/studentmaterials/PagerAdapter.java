package com.example.apix.studentmaterials;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by Apix on 21/02/2017.
 */

public class PagerAdapter extends FragmentStatePagerAdapter {

    int NumOfTabs;

    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.NumOfTabs=NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                NotesFragment notesFragment = new NotesFragment();
                return notesFragment;
            case 1:
                BooksFragment booksFragment = new BooksFragment();
                return booksFragment;
            case 2:
                PapersFragment papersFragment = new PapersFragment();
                return papersFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return NumOfTabs;
    }
}
