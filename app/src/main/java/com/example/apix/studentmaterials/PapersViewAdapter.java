package com.example.apix.studentmaterials;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Apix on 29/03/2017.
 */

public class PapersViewAdapter extends RecyclerView.Adapter<PapersViewAdapter.PapersViewHolder> {

    String year[];
    Context context;

    public PapersViewAdapter(String yr[],Context context) {
        super();
        year = yr;
        this.context = context;
    }

    public class PapersViewHolder extends RecyclerView.ViewHolder{

        TextView yearText;
        ImageView img;

        public PapersViewHolder(View itemView) {
            super(itemView);
            yearText = (TextView)itemView.findViewById(R.id.year);
            img = (ImageView)itemView.findViewById(R.id.img);

            img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context,PdfViwer.class);
                    i.putExtra("url","psample.pdf");
                    context.startActivity(i);
                }
            });
        }
    }

    @Override
    public PapersViewAdapter.PapersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(parent.getContext()).inflate(R.layout.paper_item,null);
        PapersViewHolder papersViewHolder = new PapersViewHolder(item);
        return papersViewHolder;
    }

    @Override
    public void onBindViewHolder(PapersViewAdapter.PapersViewHolder holder, int position) {
        holder.yearText.setText(year[position]);
    }

    @Override
    public int getItemCount() {
        return year.length;
    }
}
