package com.example.apix.studentmaterials;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.wang.avi.AVLoadingIndicatorView;

import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;

public class BooksFragment extends Fragment {

    RecyclerView booksRecycler;
    RecyclerView.LayoutManager layoutManager;
    BooksViewAdapter adapter;
    //RecyclerView.ItemDecoration itemDecoration;
    BooksAsyncTask asyncTask;
    AVLoadingIndicatorView avi;

    String[] Books = {"Information Technology","Electrical Engineering","Communication Skills","Linear Algebra",
                        "Development Studies", "Programming in C++","Globalization and Development"};

    String[] Authors = {"Yusuph Khamis","Michael Kramer","kelvin Spencer","Frederick John","Emily Steven",
            "Kennedy George","Phillip Kessy"};

    String[] Ratings = {"4.7","2.1","5.0","3.5","4.9","2.6","4.2"};

    Integer[] BookCovers = {R.drawable.book_1,R.drawable.book_2,R.drawable.book_3,R.drawable.book_4,
                                R.drawable.book_5,R.drawable.book_6,R.drawable.book_7};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_books, container, false);

        avi = (AVLoadingIndicatorView)rootView.findViewById(R.id.avi);



        booksRecycler = (RecyclerView) rootView.findViewById(R.id.booksRecycler);

        layoutManager = new LinearLayoutManager(getContext());


        //itemDecoration = new DividerItemDecoration(booksRecycler.getContext(),RecyclerView.VERTICAL);


        adapter = new BooksViewAdapter(Books,Authors,Ratings,BookCovers,getContext());


        asyncTask = new BooksAsyncTask();
        asyncTask.execute((Void) null);

        // Inflate the layout for this fragment
        return rootView;
    }

    public class BooksAsyncTask extends AsyncTask<Void,Void,Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            startAnim();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                // Simulate network access.
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                return false;
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            stopAnim();
            booksRecycler.setLayoutManager(layoutManager);
            //booksRecycler.addItemDecoration(itemDecoration);
            ScaleInAnimationAdapter scaleInAnimationAdapter = new ScaleInAnimationAdapter(adapter);
            scaleInAnimationAdapter.setFirstOnly(false);
            booksRecycler.setAdapter(scaleInAnimationAdapter);
        }
    }

    public void startAnim(){
        avi.show();
        // or avi.smoothToShow();
    }

    void stopAnim(){
        avi.hide();
        // or avi.smoothToHide();
    }

}
