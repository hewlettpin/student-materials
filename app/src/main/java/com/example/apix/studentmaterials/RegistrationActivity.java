package com.example.apix.studentmaterials;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;

import android.os.AsyncTask;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.dx.dxloadingbutton.lib.LoadingButton;

/**
 * A login screen that offers login via email/password.
 */
public class RegistrationActivity extends AppCompatActivity{

    private UserLoginTask mAuthTask = null;

    //First UI references.
    TextInputLayout mRegView;
    EditText regNumber;
    LoadingButton goButton;
    ImageView appLogo;
    TextView welcomeNote;
    TextView helloNote;

    //Second UI references
    TextView getStartedNote;
    ImageView profImg;
    TextView name;
    TextView birthNote;
    DatePicker datePicker;
    Button nextButton;

    //Third UI references
    TextView passwordNote;
    TextInputLayout newPassword;
    TextInputLayout rePassword;
    Button doneButton;

    //Fourth UI references
    TextView doneNote;
    ImageView welcomeImage;
    LoadingButton login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        // Set up the RegNum form.
        regNumber = (EditText)findViewById(R.id.regNumber);
        mRegView = (TextInputLayout) findViewById(R.id.regnum);
        helloNote = (TextView)findViewById(R.id.helloNote);
        Typeface typeface = Typeface.createFromAsset(getAssets(),"fonts/Pangolin-Regular.ttf");
        helloNote.setTypeface(typeface);
        appLogo = (ImageView)findViewById(R.id.appLogo);
        welcomeNote = (TextView)findViewById(R.id.welcomeNote);
        goButton = (LoadingButton) findViewById(R.id.loading_btn);

        getStartedNote = (TextView)findViewById(R.id.getStartedNote);
        profImg = (ImageView)findViewById(R.id.profImg);
        name = (TextView)findViewById(R.id.name);
        birthNote = (TextView)findViewById(R.id.birthNote);
        datePicker = (DatePicker)findViewById(R.id.datePicker);
        nextButton = (Button)findViewById(R.id.nextButton);
        getStartedNote.setAlpha(0f);
        profImg.setTranslationY(-1000f);
        name.setTranslationX(1000f);
        birthNote.setTranslationX(1000f);
        datePicker.setTranslationX(1000f);
        nextButton.setVisibility(View.INVISIBLE);

        passwordNote = (TextView)findViewById(R.id.passwordNote);
        newPassword = (TextInputLayout)findViewById(R.id.newPassword);
        rePassword = (TextInputLayout)findViewById(R.id.rePassword);
        doneButton = (Button)findViewById(R.id.doneButton);
        passwordNote.setTranslationX(1000f);
        newPassword.setTranslationX(1000f);
        rePassword.setTranslationX(1000f);
        doneButton.setVisibility(View.INVISIBLE);

        doneNote = (TextView)findViewById(R.id.doneNote);
        welcomeImage = (ImageView)findViewById(R.id.welcomeImg);
        login = (LoadingButton) findViewById(R.id.loginButton);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login.startLoading();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startActivity(new Intent(RegistrationActivity.this,LoginActivity.class));
                    }
                },2000);
            }
        });
        doneNote.setTranslationX(1000f);
        welcomeImage.setAlpha(0f);
        login.setVisibility(View.INVISIBLE);
    }

    public void fadeOne(View view){

        attemptLogin();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                welcomeNote.animate().alpha(0f).setDuration(200);
                appLogo.animate().translationYBy(-1000f).setDuration(1500);
                helloNote.animate().translationXBy(-1000f).setDuration(2000);
                mRegView.animate().translationXBy(-1000f).setDuration(1500);
                goButton.setVisibility(View.GONE);

                getStartedNote.animate().alpha(1f).setDuration(200);
                profImg.animate().translationYBy(1000f).setStartDelay(1000).setDuration(1500);
                name.animate().translationXBy(-1000f).setDuration(1800);
                birthNote.animate().translationXBy(-1000f).setDuration(2000);
                datePicker.animate().translationXBy(-1000f).setDuration(1500);
                nextButton.setVisibility(View.VISIBLE);
            }
        }, 4000);

    }

    public void fadeTwo(View view){
        birthNote.animate().translationXBy(-1000f).setDuration(2000);
        datePicker.animate().translationXBy(-1000f).setDuration(1600);
        nextButton.setVisibility(View.GONE);

        passwordNote.animate().translationXBy(-1000f).setDuration(2000);
        newPassword.animate().translationXBy(-1000f).setDuration(1700);
        rePassword.animate().translationXBy(-1000f).setDuration(1400);
        doneButton.setVisibility(View.VISIBLE);
    }

    public void fadeThree(View view){
        passwordNote.animate().translationXBy(-1000f).setDuration(2000);
        newPassword.animate().translationXBy(-1000f).setDuration(1700);
        rePassword.animate().translationXBy(-1000f).setDuration(1400);
        doneButton.setVisibility(View.GONE);

        doneNote.animate().translationXBy(-1000f).setDuration(1500);
        welcomeImage.animate().alpha(1f).setStartDelay(1500).setDuration(1000);
        login.setVisibility(View.VISIBLE);
    }

    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mRegView.setError(null);

        // Store values at the time of the login attempt.
        String regNum = regNumber.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid email address.
        if (TextUtils.isEmpty(regNum)) {
            mRegView.setError(getString(R.string.error_field_required));
            focusView = mRegView;
            cancel = true;
        } else if (!isEmailValid(regNum)) {
            mRegView.setError(getString(R.string.error_invalid_email));
            focusView = mRegView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress();
            mAuthTask = new UserLoginTask(regNum);
            mAuthTask.execute((Void) null);
        }
    }

    private boolean isEmailValid(String regN) {
        //TODO: Replace this with your own logic
        return regN.startsWith("T/UDOM/");
    }

    private void showProgress() {
        goButton.startLoading();
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        String regNum;
        //private final String mPassword;

        UserLoginTask(String reg) {
            regNum = reg;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.

            try {
                // Simulate network access.
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                return false;
            }

            // TODO: register the new account here.
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;

            if (success) {
                successful();
            }else{
                unSuccessful();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            cancelation();
        }
    }

    public void successful(){
        goButton.loadingSuccessful();
    }

    public void unSuccessful(){
        goButton.loadingFailed();
    }

    public void reset(){
        goButton.reset();
    }

    public void cancelation(){
        goButton.cancelLoading();
    }
}

