package com.example.apix.studentmaterials;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by Apix on 12/03/2017.
 */

public class ReadLaterPagerAdapter extends FragmentStatePagerAdapter {

    int NumOfTabs;

    public ReadLaterPagerAdapter(FragmentManager fm, int NumOfTabs){
        super(fm);
        this.NumOfTabs=NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                NotesReadLaterFragment notesReadLaterFragment = new NotesReadLaterFragment();
                return notesReadLaterFragment;
            case 1:
                BooksReadLaterFragment booksReadLaterFragment = new BooksReadLaterFragment();
                return booksReadLaterFragment;
            case 2:
                PapersReadLaterFragment papersReadLaterFragment = new PapersReadLaterFragment();
                return papersReadLaterFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return NumOfTabs;
    }
}
