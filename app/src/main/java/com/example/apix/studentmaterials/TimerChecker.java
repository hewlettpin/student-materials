package com.example.apix.studentmaterials;

import android.content.Context;
import android.os.SystemClock;
import android.util.Log;
import android.widget.Chronometer;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Apix on 24/03/2017.
 */

public class TimerChecker {

    static Chronometer chronometer;
    long currerntTime;
    long displayTime;
    static Timer timer;
    static Context mContext;
    UpdateListener updateListener;

    private static final TimerChecker ourInstance = new TimerChecker();

    public static TimerChecker getInstance() {
        return ourInstance;
    }

    public TimerChecker() {

    }

    public static void contextIntializer(Context contex){
        chronometer = new Chronometer(contex);
        timer = new Timer();
        mContext = contex;
    }

    public void startTimer(final TickListener tickListener) {
        long lastDatePref;
        lastDatePref = mContext.getSharedPreferences(BuildConfig.APPLICATION_ID,Context.MODE_PRIVATE).getLong("lastDate",new Date().getTime());
        String lastDate = new Date(lastDatePref).toString().split(" ")[2];
        Log.d("APP", "Date "+lastDate);


        String currentDate = new Date().toString().split(" ")[2];

        if (!(lastDate.equals(currentDate))) {
            currerntTime = 0;
            Log.d("APP", "before "+currerntTime);
        } else {
            currerntTime = mContext.getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE).getLong("lastTime", 0);
            Log.d("APP", "after "+currerntTime);

        }

        final long dummyTime = SystemClock.elapsedRealtime()/1000;
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                long timeDiff = SystemClock.elapsedRealtime()/1000 - dummyTime;
                displayTime = currerntTime + timeDiff;
                tickListener.onTick(displayTime);
                if(updateListener != null){
                    updateListener.onUpdate(timerfunction(displayTime));
                }
            }
        },0,1000);

        Log.d("APP", "started");

    }

    public void getTime(UpdateListener listener){
        updateListener = listener;
    }

    private String timerfunction(long secondsleft){
        int hours = (int) secondsleft/3600;
        long secsleftAfterhrs =  (secondsleft - hours*3600);
        int minutes = (int)secsleftAfterhrs/60;
        long seconds = secsleftAfterhrs - minutes*60;

        String hoursString = Integer.toString(hours);
        String minutesString = Integer.toString(minutes);
        String secondsString = Long.toString(seconds);

        if (seconds<=9){
            secondsString ="0"+secondsString;
        }
        if (minutes<=9){
            minutesString ="0"+minutesString;
        }
        if (hours<=9){
            hoursString ="0"+hoursString;
        }
        return (hoursString+":"+minutesString+":"+secondsString);
    }

    public void saveTime(){
        //long time = SystemClock.elapsedRealtime()/1000 - currerntTime;
        Log.d("APP", "stopped");
        mContext.getSharedPreferences(BuildConfig.APPLICATION_ID,Context.MODE_PRIVATE)
                .edit()
                .putLong("lastTime",displayTime)
                .putLong("lastDate",new Date().getTime()).apply();
        //timer.cancel();
    }

    interface TickListener {
        void onTick(long baseTime);
    }

    interface UpdateListener {
        void onUpdate(String time);
    }

}

