package com.example.apix.studentmaterials;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.apix.studentmaterials.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class BooksReadLaterFragment extends Fragment {


    public BooksReadLaterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_books_read_later, container, false);
    }

}
