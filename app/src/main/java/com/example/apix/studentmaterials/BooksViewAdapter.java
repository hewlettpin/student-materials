package com.example.apix.studentmaterials;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Apix on 23/02/2017.
 */

public class BooksViewAdapter extends RecyclerView.Adapter<BooksViewAdapter.BookViewHolder> {

    Context context;

    String[] Books;

    String[] Authors;

    String[] Ratings;

    Integer[] BookCovers;

    BooksViewAdapter(String[] Books,String[] Authors,String[] Ratings,Integer[] BookCovers,Context cont) {
        super();
        this.Books = Books;
        this.Authors = Authors;
        this.Ratings = Ratings;
        this.BookCovers = BookCovers;
        context = cont;
    }

    public class BookViewHolder extends RecyclerView.ViewHolder{

        ImageView bookCover;
        TextView bookTitle;
        TextView author;
        ImageView star;
        TextView rating;
        TextView description;
        TextView read;
        //Animation animZoomIn = AnimationUtils.loadAnimation(getApplicationContext(),
         //       R.animator.zoom_in);

        public BookViewHolder(final View itemView) {
            super(itemView);
            bookCover = (ImageView)itemView.findViewById(R.id.bookItem);
            bookTitle = (TextView)itemView.findViewById(R.id.bookTitle);
            author = (TextView)itemView.findViewById(R.id.author);
            star = (ImageView)itemView.findViewById(R.id.star);
            rating = (TextView)itemView.findViewById(R.id.rating);
            description = (TextView)itemView.findViewById(R.id.description);
            read = (TextView)itemView.findViewById(R.id.read);

            description.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context,BooksDecriptionActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        context.startActivity(intent, ActivityOptions.makeSceneTransitionAnimation((Activity)context).toBundle());
                    }
                }
            });

            read.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context,PdfViwer.class);
                    i.putExtra("url","sample.pdf");
                    context.startActivity(i);
                }
            });
        }
    }

    @Override
    public BooksViewAdapter.BookViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(parent.getContext()).inflate(R.layout.notes_item,null);
        BookViewHolder bookViewHolder = new BookViewHolder(item);
        return bookViewHolder;
    }

    @Override
    public void onBindViewHolder(BooksViewAdapter.BookViewHolder holder, int position) {
        holder.bookCover.setImageResource(BookCovers[position]);
        holder.bookTitle.setText(Books[position]);
        holder.author.setText("Author: "+Authors[position]);
        holder.rating.setText(Ratings[position]);
    }

    @Override
    public int getItemCount() {
        return Books.length;
    }
}
