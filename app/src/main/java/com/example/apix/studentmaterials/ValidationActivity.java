package com.example.apix.studentmaterials;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ValidationActivity extends AppCompatActivity {

    Button getstarted;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_validation);

        textView = (TextView)findViewById(R.id.textView);
        Typeface typeface = Typeface.createFromAsset(getAssets(),"fonts/Pangolin-Regular.ttf");
        textView.setTypeface(typeface);

        getstarted = (Button)findViewById(R.id.getstarted);
        getstarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ValidationActivity.this,LoginActivity.class));
            }
        });
    }
}
