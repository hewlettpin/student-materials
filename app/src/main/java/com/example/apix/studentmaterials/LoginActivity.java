package com.example.apix.studentmaterials;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dx.dxloadingbutton.lib.LoadingButton;

/**
 * Created by Apix on 12/02/2017.
 */

public class LoginActivity extends AppCompatActivity{

    boolean doubleBackToExitPressedOnce = false;

    String[] vyuo = {"Click to Select University","University of Dar es Salaam","University of Dodoma","University of Mzumbe","University of Kairuki"};

    Spinner spinner;

    private StudentLoginTask mAuthTask = null;

    // UI references.
    private EditText mRegNumView;
    private EditText mPasswordView;
    LoadingButton RegloginInButton;
    TextView newUsery;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        TimerChecker.getInstance().saveTime();

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        ImageView profImg = (ImageView)findViewById(R.id.profImg);
        TextInputLayout regnum = (TextInputLayout)findViewById(R.id.regnum);
        TextInputLayout passwd = (TextInputLayout)findViewById(R.id.passwd);
        LoadingButton loading_btn = (LoadingButton)findViewById(R.id.loading_btn);
        TextView forgpassword = (TextView)findViewById(R.id.forgpassword);
        TextView newUser = (TextView)findViewById(R.id.newUser);
        profImg.setTranslationX(1000f);
        regnum.setTranslationX(1000f);
        passwd.setTranslationX(1000f);
        loading_btn.setTranslationX(1000f);
        forgpassword.setTranslationX(1000f);
        newUser.setTranslationX(1000f);

        spinner = (Spinner)findViewById(R.id.spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(),android.R.layout.simple_spinner_item,vyuo);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(vyuo[position].equals("Please select")){
                    Snackbar.make(findViewById(R.id.coordinatorVyuo),"You have to choose a University to continue",Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // Set up the login form.
        mRegNumView = (EditText) findViewById(R.id.regNumber);

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        RegloginInButton = (LoadingButton) findViewById(R.id.loading_btn);
        RegloginInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        newUsery = (TextView)findViewById(R.id.newUser);
        newUsery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this,RegistrationActivity.class));
                finish();
            }
        });

    }

    public void fade(View view){
        ImageView applogo = (ImageView)findViewById(R.id.appLogo);
        Spinner spin = (Spinner)findViewById(R.id.spinner);
        Button but = (Button)findViewById(R.id.gobtn);
        applogo.animate().translationXBy(-1000f).setDuration(2000);
        spin.animate().translationXBy(-1000f).setDuration(1500);
        but.animate().translationXBy(-1000f).setDuration(1000);

        ImageView profImg = (ImageView)findViewById(R.id.profImg);
        TextInputLayout regnum = (TextInputLayout)findViewById(R.id.regnum);
        TextInputLayout passwd = (TextInputLayout)findViewById(R.id.passwd);
        LoadingButton loading_btn = (LoadingButton)findViewById(R.id.loading_btn);
        TextView forgpassword = (TextView)findViewById(R.id.forgpassword);
        TextView newUser = (TextView)findViewById(R.id.newUser);
        profImg.animate().translationXBy(-1000f).setDuration(2000);
        regnum.animate().translationXBy(-1000f).setDuration(1800);
        passwd.animate().translationXBy(-1000f).setDuration(1600);
        loading_btn.animate().translationXBy(-1000f).setDuration(1400);
        forgpassword.animate().translationXBy(-1000f).setDuration(1200);
        newUser.animate().translationXBy(-1000f).setDuration(1000);
    }



    //Login Layout
    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mRegNumView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String regNum = mRegNumView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid Registration Number.
        if (TextUtils.isEmpty(regNum)) {
            mRegNumView.setError(getString(R.string.error_field_required));
            focusView = mRegNumView;
            cancel = true;
        } else if (!isRegNumValid(regNum)) {
            mRegNumView.setError(getString(R.string.error_invalid_email));
            focusView = mRegNumView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress();
            mAuthTask = new StudentLoginTask(regNum,password);
            mAuthTask.execute((Void) null);
        }
    }

    private boolean isRegNumValid(String regNum) {
        return regNum.startsWith("T/UDOM/");
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 8;
    }

    private void showProgress() {
        RegloginInButton.startLoading();
    }

    public class StudentLoginTask extends AsyncTask<Void, Void, Boolean> {

         String regNum;
         String password;

        StudentLoginTask(String reg, String passwd) {
            regNum = reg;
            password = passwd;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.

            try {
                // Simulate network access.
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                return false;
            }

            // TODO: register the new account here.
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;

            if (success) {
                RegloginInButton.loadingSuccessful();
               // new StartActivityTask().execute();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startActivity(new Intent(LoginActivity.this,MainActivity.class));
                        finish();
                    }
                }, 1000);
            } else {
                RegloginInButton.loadingFailed();
                mPasswordView.setError(getString(R.string.error_incorrect_password));
                mPasswordView.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            RegloginInButton.cancelLoading();
        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Press BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

}
