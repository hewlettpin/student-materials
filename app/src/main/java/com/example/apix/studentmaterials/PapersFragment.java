package com.example.apix.studentmaterials;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.wang.avi.AVLoadingIndicatorView;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;

public class PapersFragment extends Fragment {

    String years[] = {"2017","2016","2015","2014","2013","2012","2011","2010"};
    AVLoadingIndicatorView avi;
    AVLoadingIndicatorView avi2;
    PapersAsyncTask asyncTask;
    View rootView;
    RecyclerView UErecycler;
    RecyclerView Testrecycler;
    PapersViewAdapter adapter;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.LayoutManager layoutManager2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_papers, container, false);

        adapter = new PapersViewAdapter(years,getContext());
        layoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false);
        layoutManager2 = new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false);

        UErecycler = (RecyclerView) rootView.findViewById(R.id.UErecyclerView);

        Testrecycler = (RecyclerView) rootView.findViewById(R.id.TestRecyclerView);

        avi = (AVLoadingIndicatorView)rootView.findViewById(R.id.avi);

        avi2 = (AVLoadingIndicatorView)rootView.findViewById(R.id.avi2);

        asyncTask = new PapersAsyncTask();
        asyncTask.execute((Void) null);

        return rootView;
    }

    public class PapersAsyncTask extends AsyncTask<Void,Void,Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            startAnim();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                // Simulate network access.
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                return false;
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            stopAnim();
            UErecycler.setLayoutManager(layoutManager);
           // UErecycler.setItemAnimator(new ScaleInAnimator());
            ScaleInAnimationAdapter scaleInAnimationAdapter = new ScaleInAnimationAdapter(adapter);
            scaleInAnimationAdapter.setFirstOnly(false);
            UErecycler.setAdapter(scaleInAnimationAdapter);

            Testrecycler.setLayoutManager(layoutManager2);
            Testrecycler.setAdapter(scaleInAnimationAdapter);
        }
    }

    public void startAnim(){
        avi.show();
        avi2.show();
        // or avi.smoothToShow();
    }

    void stopAnim(){
        avi.hide();
        avi2.hide();
        // or avi.smoothToHide();
    }

}
