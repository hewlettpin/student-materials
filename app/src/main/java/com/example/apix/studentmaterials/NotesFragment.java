package com.example.apix.studentmaterials;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.apix.studentmaterials.R;
import com.wang.avi.AVLoadingIndicatorView;

public class NotesFragment extends Fragment {

    TextView textView;
    View rootView;
    AVLoadingIndicatorView avi;
    NotesAsyncTask asyncTask;

    boolean state = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_notes, container, false);

        textView = (TextView)rootView.findViewById(R.id.notes);
        textView.setVisibility(View.GONE);

        avi = (AVLoadingIndicatorView)rootView.findViewById(R.id.avi);


        asyncTask = new NotesAsyncTask();
        asyncTask.execute((Void) null);

        Log.d("TAG", "onCreateView: changingSpinnerCreate: "+state);

         return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 112){
            Log.d("TAG", "onActivityResult: here");
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.detach(this).attach(this).commit();
        }
    }

    public class NotesAsyncTask extends AsyncTask<Void,Void,Boolean>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            startAnim();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                // Simulate network access.
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                return false;
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            stopAnim();
            textView.setVisibility(View.VISIBLE);
        }
    }

    public void startAnim(){
        avi.show();
        // or avi.smoothToShow();
    }

    void stopAnim(){
        avi.hide();
        // or avi.smoothToHide();
    }

    public void changingSpinner(boolean state){
        Log.d("TAG", "changingSpinnerBefore: "+state);
        this.state = state;
        Log.d("TAG", "changingSpinnerAfter: "+state);
    }
}
