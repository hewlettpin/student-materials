package com.example.apix.studentmaterials;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.transition.Explode;
import android.transition.Fade;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.eugeneek.smilebar.SmileBar;

public class BooksDecriptionActivity extends AppCompatActivity {

    TextView rate;
    TextView otherBooks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
            setupWindowAnimation();
        }
        setContentView(R.layout.activity_books_decription);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
//        }

        rate = (TextView)findViewById(R.id.read);
        rate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog rankDialog = new Dialog(BooksDecriptionActivity.this,R.style.FullHeightDialog);
                rankDialog.setContentView(R.layout.rating_dialog);
                rankDialog.setCancelable(true);
                SmileBar smileBar = (SmileBar)rankDialog.findViewById(R.id.SmileBar);
                smileBar.setOnRatingSliderChangeListener(new SmileBar.OnRatingSliderChangeListener() {
                    @Override
                    public void onPendingRating(int rating) {
                        Log.i("onPendingRating", "" + rating);
                       // btn.setText("" + rating);
                    }

                    @Override
                    public void onFinalRating(int rating) {
                        Log.i("onFinalRating", "" + rating);
                    }

                    @Override
                    public void onCancelRating() {
                        Log.i("onCancelRating", "cancel");
                    }
                });
                smileBar.setRating(smileBar.getRating());


                TextView rateBtn = (TextView) rankDialog.findViewById(R.id.rateBtn);
                rateBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        rankDialog.dismiss();
                        Toast.makeText(getApplicationContext(),"Thanks for rating the book",Toast.LENGTH_SHORT).show();
                    }
                });
                rankDialog.show();
            }
        });

        otherBooks = (TextView)findViewById(R.id.otherBooks);
        otherBooks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @TargetApi(21)
    public void setupWindowAnimation(){
        Fade explode = new Fade();
        explode.setDuration(1000);
        getWindow().setEnterTransition(explode);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}